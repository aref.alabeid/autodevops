# Autodevops
Auto DevOps detects your programming language and uses CI/CD components to create and run default pipelines to build and test your application. 

## Autodevops components

### build 

`build` creates a build of the application using an existing Dockerfile or Heroku buildpacks. The resulting Docker image is pushed to the Container Registry, and tagged with the commit SHA or tag. 

```yaml
include:
  - component: gitlab.com/components/autodevops/build@<VERSION>
  inputs:
    stage: build
```

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `registry_host` | `$CI_TEMPLATE_REGISTRY_HOST` | The GitLab registry host to use to pull the container image |
| `auto_build_image_version` | v1.51.0 |  |
| `stage` | build | The pipeline stage where to add the review deployment job |
|`output_image_path`| The path of the image output. |
|`output_image_tag`| The tag of the image output. |

## Contribute

Please read about CI/CD components and best practices at: https://docs.gitlab.com/ee/ci/components 